#!/usr/bin/env bash
#
# Deploy AWS ask CLI, https://developer.amazon.com/en-US/docs/alexa/smapi/ask-cli-intro.html
#
# Required globals:
#   AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY or AWS_OIDC_ROLE_ARN
#   AWS_DEFAULT_REGION
#   ASK_REFRESH_TOKEN
#   ASK_VENDOR_ID
#   ENVIRONMENT

# Optional globals:
#   DEBUG
#   EXTRA_ARGS

umask 000
source "$(dirname "$0")/common.sh"

# mandatory parameters
ASK_REFRESH_TOKEN="${ASK_REFRESH_TOKEN:?'ASK_REFRESH_TOKEN variable missing.'}"
ASK_VENDOR_ID="${ASK_VENDOR_ID:?'ASK_VENDOR_ID variable missing.'}"

default_authentication() {
  info "Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY."
  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
}

oidc_authentication() {
  info "Authenticating with a OpenID Connect (OIDC) Web Identity Provider."
      mkdir -p /.aws-oidc
      AWS_WEB_IDENTITY_TOKEN_FILE=/.aws-oidc/web_identity_token
      echo "${BITBUCKET_STEP_OIDC_TOKEN}" >> ${AWS_WEB_IDENTITY_TOKEN_FILE}
      chmod 400 ${AWS_WEB_IDENTITY_TOKEN_FILE}
      aws configure set web_identity_token_file ${AWS_WEB_IDENTITY_TOKEN_FILE}
      aws configure set role_arn ${AWS_OIDC_ROLE_ARN}
      export AWS_PROFILE="default"
      unset AWS_ACCESS_KEY_ID
      unset AWS_SECRET_ACCESS_KEY
}

setup_authentication() {
  enable_debug
  AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
  if [[ -n "${AWS_OIDC_ROLE_ARN}" ]]; then
    if [[ -n "${BITBUCKET_STEP_OIDC_TOKEN}" ]]; then
      oidc_authentication
    else
      warning 'Parameter `oidc: true` in the step configuration is required for OIDC authentication'
      default_authentication
    fi
  else
    default_authentication
  fi
}

preexecution_hook() {
  if [[ -n "${PRE_EXECUTION_SCRIPT}" ]]; then
    if [[ ! -f "${PRE_EXECUTION_SCRIPT}" ]]; then
      fail "$PRE_EXECUTION_SCRIPT preexecution hook file doesn't exist."
    fi
    ./$PRE_EXECUTION_SCRIPT
  fi
}

environment_file_copy() {
  if [[ -n "${ENVIRONMENT}" ]]; then
    find . -name "*\.${ENVIRONMENT}\.*" | while read -r filename; do cp "${filename}" "${filename/.${ENVIRONMENT}./.}"; done
  fi
}

environment_file_copy
setup_authentication
preexecution_hook

# default parameters
EXTRA_ARGS=${EXTRA_ARGS:=""}

AWS_DEBUG_ARGS=""
if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling debug mode."
  AWS_DEBUG_ARGS="--debug"
fi

# Build the arguments string
ARGS_STRING=""
ARGS_STRING+="${EXTRA_ARGS}"

info "Starting ask deployment..."
export ASK_REFRESH_TOKEN ASK_VENDOR_ID
run ask deploy ${ARGS_STRING} ${AWS_DEBUG_ARGS}
if [[ "${status}" -eq 0 ]]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi
