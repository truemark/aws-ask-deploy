FROM amazon/aws-cli:latest

RUN curl -fsSL -o /common.sh https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh && \
  curl -fsSL https://rpm.nodesource.com/setup_14.x | bash - && \
  yum install -y nodejs && \
  npm install -g ask-cli && \
  yum clean all && \
  rm -rf /var/cache/yum

# Temporary fix to support AWS_PROFILE
#COPY profile-helper.js /usr/lib/node_modules/ask-cli/lib/utils/profile-helper.js
COPY pipe /
COPY LICENSE.txt README.md /

ENTRYPOINT ["/pipe.sh"]
